package com.example.assignment.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.assignment.Class.ThuChi;
import com.example.assignment.R;

import java.util.ArrayList;


public class AdapterRecyView extends RecyclerView.Adapter<AdapterRecyView.ViewHolder> {
    ArrayList<ThuChi> thuChis;
    Context context;
    LayoutInflater inflater;

    public AdapterRecyView(ArrayList<ThuChi> thuChis, Context context) {
        this.thuChis = thuChis;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }



    @NonNull
    @Override
    public AdapterRecyView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemview = inflater.inflate(R.layout.item_qlt,parent,false);
        return new ViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecyView.ViewHolder holder, int position) {
        ThuChi thuChi = thuChis.get(position);
        holder.tvnameLuong.setText(thuChi.getName());

    }

    @Override
    public int getItemCount() {
        return thuChis.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvnameLuong;
        ImageView imgDelete;
        ImageView imgSua;

        public ViewHolder(View itemView) {
            super(itemView);
            tvnameLuong = itemView.findViewById(R.id.tvnameLuong);
            imgSua = itemView.findViewById(R.id.itemSua);
            imgDelete = itemView.findViewById(R.id.itemDelete);
        }
    }
}